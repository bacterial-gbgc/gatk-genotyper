---
title: GATK Genotyper
author: Samuel Barreto
---

# Gatk Genotyper

This script is a simple wrapper around the GATK best practices.

## USAGE:

    gatk-genotyper [options] -- <sample1> <sample2> ... <sampleN>

## DESCRIPTION:

gatk-genotyper wraps the GATK Best Practices for variant calling
a cohort of samples. It maps the reads of all _sample_ in [input] to
the reference sequence using bwa, sort and tag the reads appropriately
and call variants using HaplotypeCaller in GVCF mode. GVCF are then
combined together and variants are called jointly by comparing
alignment evidence to the reference sequence.

## OPTIONS:

- `<sample1> ...`: Prefix of samples to be mapped, assuming reads are named [input]/_sampleN_\_1.fastq.gz and [input]/_sampleN_\_2.fastq.gz, all output files will be [output]/_sampleN_
- `-i`, `--input`: input directory (default: 'input')
- `-o`, `--output`: output directory (default: 'output')
- `-r`, `--reference`: Reference sequence in fasta format (default: 'data/ref.fasta')
- `-p`, `--prefix`: prefix of the output file (default: 'cohort')
- `-n`, `--dryrun`: Show output message only, don't really run command (default: 'off')
- `-m`, `--makeopt`: Options passed to 'make' (no default)
- `-h`, `--help`: Display this help message
- `-H`, `--HELP`: Display a more detailed program description

## AUTHOR:

Samuel Barreto, (2018-11-19)

## DETAILS:

The gatk-genotyper script is a simple wrapper around the GATK tool
for variant calling a set of sample. It sticks to the GATK Best
Practices as best as it can, with the exception that GATK relies
a lot on available data for commonly mapped genomes, like human,
mouse or nematodes. When working with non-model organisms, the
recalibration can not be done since it relies on shared database
of known sequencing errors and artifacts.

The genotyping is done in the following way (one can parse the code in
{prefix}/share/gatk-genotyper/call-variants.mk to get the detail.):


1. The reference sequence is indexed by bwa, samtools and GATK
CreateSequenceDictionary. The reference sequence is assumed to be in
the data/ref.fasta file if not explicitely specified using '-r' or
'--reference'.


2. Reads in the [input] folder ('input' by default, unless
explicitely stated using '-i' or '--input') are converted to an
umapped BAM file, (ubam extention) with readgroups and sample
names (SM and RG tags) set to the _sample_ prefix. That means
that for a sample _X_ with reads forward and reverse
[input]/_X_\_1.fastq.gz and [input]/_X_\_2.fastq.gz, a file
[output]/_X_.ubam will be created.


3. Unmapped reads are mapped to the reference sequence using bwa
mem with the same default as advised by GATK best practices,
and converted to bam. A file [output]/_X_.bam will be created.


4. Mapped reads are merged back together with the unmapped reads
using GATK MergeBamAlignment, with same parameters as in the
default pipeline for GATK Reads preparation.


5. Duplicate reads are marked via GATK MarkDuplicates. Metrics are
stored in the .log/_X_.mark_dup.metrics.txt file.


6. Marked reads are sorted by coordinate along the genome using
GATK SortSam, and tagged in their NM, MD and UQ tags using GATK
SetNmMdAndUqTags. A _X_.tag.sorted.bam is created, indexed.
This file will be preserved at the end of the pipeline.


7. Mapped reads are used to call variants with GATK
HaplotypeCaller in GVCF mode. Two files are produced,
[output]/_X_.g.vcf, a GVCF file with proper GATK-specific
annotation for joint genotyping; and [output]/_X_.remapped.bam
file for inspection of local reads reassembly of GATK.


8. For each sample _X_, _Y_ and _Z_, all _X_.g.vcf _Y_.g.vcf and
_Z_.g.vcf are combined together by GATK CombineGVCFs, and
genotyped using GATK GenotypeGVCFs to produce a file called
[output]/_prefix_.vcf, where prefix is set to 'cohort' by
default, unless explicitely stated with '-p' or '--prefix'.


All these steps are managed by a make invocation, so that no
unnecessary steps are performed. This allows for easy tweaking of
parameters.

For debugging purpose or just to see if the pipeline is setup
correctly, use the '-n' or '--dryrun' flag. No command will be
actually run, they will only be echoed on the screen.

When you are ready to actually run the pipeline, you can adjust
the number of parallel job with the '-m' or '--makopt' flag. Wrap
its argument in quotes (see examples below).


## EXAMPLES:

    # For demonstration purposes:
    $ mkdir -p {data,input,output}
    $ touch data/ref.fasta input/sample{1,2,3}_{1,2}.fastq.gz

    # Which gives the following directory structure:
    # .
    # ├── data
    # │   └── ref.fasta
    # ├── input
    # │   ├── sample1\_1.fastq.gz
    # │   ├── sample1\_2.fastq.gz
    # │   ├── sample2\_1.fastq.gz
    # │   ├── sample2\_2.fastq.gz
    # │   ├── sample3\_1.fastq.gz
    # │   └── sample3\_2.fastq.gz
    # └── output

    # Run the following command to get an idea of actual steps
    # performed:
    $ gatk-genotyper -n -- sample{1,2,3}

    # Adjust the makefile options using -m or --makeopt
    $ gatk-genotyper -n -m "-j2 THREADS=2" -- sample{1,2,3}

    # (By default, make run only one job sequentially; that can be
    # adjusted using -jN where N is the number of parallel tasks. Set
    # it to 2 if you want two samples to be genotyped in parallel for
    # example. Moreover, the number of THREADS for genotyping is set
    # to 4 by default. Reduce or increase it with THREADS=N.)

# INSTALLATION

Source code can be downloaded from
[here](https://gitlab.com/bacterial-gbgc/gatk-genotyper/tags/v1.0).
Since gatk-genotyper is a simple bash script, installation is quite
straightforward. Installation instructions are detailed in the linked
release page.

## Prerequisites

Those three programs are required for gatk-genotyper to run. The
`./configure` script checks that they are available in the path.

- GATK >= v4.0
- bwa
- samtools

# License

GPL3+

;; Local Variables:
;; mode: markdown
;; End:
